package obrada;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class Connect {
	
	public static Connection connect() {
		Connection conn = null;	
		try {		
			//String url = "jdbc:sqlite:src/baza";	
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/baza_podataka", "root", "1234");			
			System.out.println("Veza uspostavljena");

		}catch(SQLException e) {			
			System.out.println(e.getMessage());		
		}finally {  
            try {  
                if (conn == null) {  
                    conn.close();  
                }  
            } catch (SQLException ex) {  
            	System.out.println("1");  
            	System.out.println(ex.getMessage());  
            	System.out.println("2");  
            }  
        }
		return conn; 
    }  

    public static void main(String[] args) {  
        connect();  
    }  

}  
