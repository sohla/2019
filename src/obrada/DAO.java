package obrada;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modeli.Lekar;
import modeli.MedicinskaSestra;
import modeli.Pacijent;
import modeli.Pregled;
import obrada.Connect;

public class DAO {
	
	private ArrayList<Lekar> lekari; 
	private ArrayList<Pacijent> pacijenti;
	private ArrayList<MedicinskaSestra> medicinskeSestre;
	private ArrayList<Pregled> pregledi;
	private ArrayList<Pregled> preglediLekara;
	private ArrayList<Pregled> preglediPacijenta;
	
	public DAO() {
		this.lekari = new ArrayList<Lekar>();
		this.pacijenti = new ArrayList<Pacijent>();
		this.medicinskeSestre = new ArrayList<MedicinskaSestra>();
		this.pregledi = new ArrayList<Pregled>();
		this.preglediLekara = new ArrayList<Pregled>();
		this.preglediPacijenta = new ArrayList<Pregled>();
	}
	
	public ArrayList<Pacijent> getPacijenti() {
		return pacijenti;
		
	}
	public ArrayList<Lekar> getLekari() {
		return lekari;
		
	}
	public ArrayList<MedicinskaSestra> getMedinickeSestre() {
		return medicinskeSestre;
		
	}
	
	
	public ArrayList<Pregled> getPregledi() {
		return pregledi;
		
	} 
	
	public ArrayList<Pregled> getPreglediLekara() {
		return preglediLekara;
		
	} 	
	
	public ArrayList<Pregled> getPreglediPacijenta() {
		return preglediPacijenta;
		
	} 	
	
	public ArrayList<Pacijent> getAllPacijent() throws SQLException {
			
		Connection conn = Connect.connect();	
		try {
			PreparedStatement pstm = conn.prepareStatement("SELECT * FROM pacijent");
			ResultSet rs = pstm.executeQuery();
			while(rs.next()) {
				Pacijent pac = new Pacijent("","","","","","","","","",0,"","");
				
				pac.setIme(rs.getString("ime")); 
				pac.setPrezime(rs.getString("prezime"));
				pac.setJBMG(rs.getString("JMBG"));
				pac.setPol(rs.getString("pol"));
				pac.setAdresa(rs.getString("adresa"));
				pac.setBrojTelefona(rs.getString("brojTelefona"));
				pac.setKorisnicko(rs.getString("korisnicko"));
				pac.setLozinka(rs.getString("lozinka"));
				pac.setLekar(rs.getString("Lekar"));
				pac.setBroj(rs.getInt("broj"));
				pac.setDatumIsteka(rs.getDate("datumIsteka"));
				pac.setKategorija(rs.getString("kategorija"));
				System.out.print(pac);
				pacijenti.add(pac);
			}
			
			}catch(SQLException e) {
				System.out.println(e);
			}
		return pacijenti;
	}
	
	public void createPacijent(String ime,String prezime,int JMBG,String pol, String adresa, int brTelefona,String korisnicko,String lozinka,String lekar, int broj,Date datumIsteka, String kategorija) throws SQLException  {
		
		Connection conn = null;
		Statement stmt = conn.createStatement();
		
		try {	
			PreparedStatement ps = conn.prepareStatement("INSERT INTO pacijenti values(?,?,?,?,?,?,?,?,?,?,?,?");
			ps.setString(1, ime);
			ps.setString(2, prezime);
			ps.setInt(3, JMBG);
			ps.setString(4, pol);			
			ps.setString(5, adresa);
			ps.setInt(6, brTelefona);			
			ps.setString(7, korisnicko);
			ps.setString(8, lozinka);			
			ps.setString(9, ime);
			ps.setString(10, prezime);
			ps.setString(11, ime);
			ps.setString(12, prezime);
			int i = ps.executeUpdate();
			if (i != 0) {
				System.out.println("Created");
			}else {
				System.out.println("Not created");
			}
	
		}catch(Exception e) {
			System.out.println(e);
		}		
	}
	
	public void updatePacijent(String ime,String prezime,int JMBG,String pol, String adresa, int brTelefona,String korisnicko,String lozinka,String lekar ,int broj, Date datumIsteka,String kategorija) {
		
		Connection conn = null;
		
		try {
			conn = Connect.connect();
			PreparedStatement ps = conn.prepareStatement("UPDATE Pacijenti SET ime=?, prezime=?, JMBG=?, pol=?, adresa=?, brojTelefona=?, koriscniko=?, lozinka=?, lekar=?, broj=?, datumIsteka=?, kategorija=? WHERE JMBG=?");
			ps.setString(1, ime);
			ps.setString(2, prezime);
			ps.setInt(3, JMBG);
			ps.setString(4, pol);			
			ps.setString(5, adresa);
			ps.setInt(6, brTelefona);			
			ps.setString(7, korisnicko);
			ps.setString(8, lozinka);			
			ps.setString(9, lekar);
			ps.setInt(10, broj);
			ps.setDate(10, datumIsteka);
			ps.setString(12, kategorija);
			int i = ps.executeUpdate();
			if (i != 0) {
				System.out.println("Created");
			}else {
				System.out.println("Not created");
			}

		}catch(Exception e) {
			System.out.println(e);
		}			
	}
	
	public  Pacijent loginPacijent(String korisnickoIme, String sifra) {
		for (Pacijent pacijent : pacijenti) {
			if(pacijent.getKorisnicko().equalsIgnoreCase(korisnickoIme) && pacijent.getLozinka().equalsIgnoreCase(sifra)) {
				return pacijent;
			}
		}
		return null;
	}

	public void removePacijent(Pacijent pacijent) {
	
	}
	
	public void getByIDPacijent(Pacijent pacijent) {
	
	}	
	
	public ArrayList<Lekar> getAllLekar() throws SQLException {
		
		Connection conn = Connect.connect();
		
		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM lekar");
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Lekar lek = new Lekar("","","","","","","","", (double) 0, "","");	
				lek.setIme(rs.getString("ime"));
				lek.setPrezime(rs.getString("prezime"));
				lek.setJBMG(rs.getString("JMBG"));
				lek.setPol(rs.getString("pol"));
				lek.setAdresa(rs.getString("adresa"));
				lek.setBrojTelefona(rs.getString("brojTelefona"));
				lek.setKorisnicko(rs.getString("korisnicko"));
				lek.setLozinka(rs.getString("lozinka"));
				
				lek.setPlata(rs.getDouble("plata"));
				lek.setSpecijalizacija(rs.getString("specijalizacija"));
				lek.setSluzba(rs.getString("sluzba"));

				lekari.add(lek);
				System.out.print(lek);
			}
		
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return lekari;

	}

	public void createLekar(String ime,String prezime,String JMBG,String pol, String adresa, int brTelefona,String korisnicko,String lozinka, int plata, String specijalizacija,String sluzba) throws SQLException  {
		
		Connection conn = null;
		Statement stmt = conn.createStatement();
		
		try {
			conn = Connect.connect();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO Lekari values(?,?,?,?,?,?,?,?,?,?,?,?");
			ps.setString(1, ime);
			ps.setString(2, prezime);
			ps.setString(3, JMBG);
			ps.setString(4, pol);			
			ps.setString(5, adresa);
			ps.setInt(6, brTelefona);			
			ps.setString(7, korisnicko);
			ps.setString(8, lozinka);			
			ps.setDouble(9, plata);
			ps.setString(10, specijalizacija);
			ps.setString(11, sluzba);
			int i = ps.executeUpdate();
			if (i != 0) {
				System.out.println("Created");
			}else {
				System.out.println("Not created");
			}

		}catch(Exception e) {
			System.out.println(e);
		}
			
	}
	
	public void updateLekar(String ime,String prezime,int JMBG,String pol, String adresa, int brTelefona,String korisnicko,String lozinka, int plata, String specijalizacija,String sluzba) {
		
		Connection conn = null;
		
		try {	
			conn = Connect.connect();
			PreparedStatement ps = conn.prepareStatement("UPDATE Lekari SET ime=?, prezime=?, JMBG=?, pol=?, adresa=?, brojTelefona=?, koriscniko=?, lozinka=?, plata=?, specijalizacija=?, sluzba=? WHERE JMBG=?");
			ps.setString(1, ime);
			ps.setString(2, prezime);
			ps.setInt(3, JMBG);
			ps.setString(4, pol);			
			ps.setString(5, adresa);
			ps.setInt(6, brTelefona);			
			ps.setString(7, korisnicko);
			ps.setString(8, lozinka);			
			ps.setDouble(9, plata);
			ps.setString(10, specijalizacija);
			ps.setString(11, sluzba);
			int i = ps.executeUpdate();
			if (i != 0) {
				System.out.println("Created");
			}else {
				System.out.println("Not created");
			}
	
		}catch(Exception e) {
			System.out.println(e);
		}		
	
	}
	
	public  Lekar loginLekar(String korisnickoIme, String sifra) {
		for (Lekar lekar : lekari) {
			if(lekar.getKorisnicko().equalsIgnoreCase(korisnickoIme) && lekar.getLozinka().equalsIgnoreCase(sifra)) {
				return lekar;
			}
		}
		return null;
	}
	
	public void removeLekar(Lekar Lekar) {
		
		lekari.remove(Lekar);
	}
	
	public void getByIDLekar(Lekar Lekar) {

	}
	
	public ArrayList<MedicinskaSestra> getAllMedicinskaSestra() throws SQLException {	
		
		Connection conn = Connect.connect();
		
		try {
				PreparedStatement stmt  = conn.prepareStatement("SELECT * FROM  medicinska");
				ResultSet rs= stmt.executeQuery(); 
				while(rs.next()) {
					MedicinskaSestra med = new MedicinskaSestra("","","","","","","","",(double) 0,"");			
					med.setIme(rs.getString("ime"));
					med.setPrezime(rs.getString("prezime"));
					med.setJBMG(rs.getString("JMBG"));
					med.setPol(rs.getString("pol"));
					med.setAdresa(rs.getString("adresa"));
					med.setBrojTelefona(rs.getString("brojTelefona"));
					med.setKorisnicko(rs.getString("korisnicko"));
					med.setLozinka(rs.getString("lozinka"));
					
					med.setPlata(rs.getDouble("plata"));
					med.setSluzba(rs.getString("sluzva"));
				
					medicinskeSestre.add(med); 
					System.out.print(med);
			
				}
			}catch (SQLException e) {
	            System.out.println(e.getMessage());
		}
		return medicinskeSestre;

		}

	public void createMedicinskaSestra(String ime,String prezime,int JMBG,String pol, String adresa, int brTelefona,String korisnicko,String lozinka, int plata,String sluzba) throws SQLException  {
		
		Connection conn = null;
		Statement stmt = conn.createStatement();
		
		try {
			PreparedStatement ps = conn.prepareStatement("INSERT INTO medicinske values(?,?,?,?,?,?,?,?,?,?,?");
			ps.setString(1, ime);
			ps.setString(2, prezime);
			ps.setInt(3, JMBG);
			ps.setString(4, pol);			
			ps.setString(5, adresa);
			ps.setInt(6, brTelefona);			
			ps.setString(7, korisnicko);
			ps.setString(8, lozinka);			
			ps.setDouble(9, plata);
			ps.setString(11, sluzba);
			int i = ps.executeUpdate();
			if (i != 0) {
				System.out.println("Created");
			}else {
				System.out.println("Not created");
			}
	
		}catch(Exception e) {
			System.out.println(e);
		}

	}
	
	public void updateMedicinskaSestra(String ime,String prezime,int JMBG,String pol, String adresa, int brTelefona,String korisnicko,String lozinka, int plata, String specijalizacija,String sluzba) {
		
		Connection conn = null;
		
		try {	
			conn = Connect.connect();
			PreparedStatement ps = conn.prepareStatement("UPDATE medicinske SET ime=?, prezime=?, JMBG=?, pol=?, adresa=?, brojTelefona=?, koriscniko=?, lozinka=?, plata=?, sluzba=? WHERE JMBG=?");
			ps.setString(1, ime);
			ps.setString(2, prezime);
			ps.setInt(3, JMBG);
			ps.setString(4, pol);			
			ps.setString(5, adresa);
			ps.setInt(6, brTelefona);			
			ps.setString(7, korisnicko);
			ps.setString(8, lozinka);			
			ps.setDouble(9, plata);
			ps.setString(10, sluzba);
			int i = ps.executeUpdate();
			if (i != 0) {
				System.out.println("Created");
			}else {
				System.out.println("Not created");
			}

		}catch(Exception e) {
			System.out.println(e);
		}		
	
	}
	
	public MedicinskaSestra loginMedicinskaSestra(String korisnickoIme, String sifra) {
		for (MedicinskaSestra ms : medicinskeSestre) {
			if(ms.getKorisnicko().equalsIgnoreCase(korisnickoIme) 
					&& ms.getLozinka().equalsIgnoreCase(sifra)) {
				return ms;
			}
		}
		return null;
	}

	public void removeMedicinskaSestra(MedicinskaSestra MedicinskaSestra) {
		
		medicinskeSestre.remove(MedicinskaSestra);
		
	}
	
	public void getByIDMedicinskaSestra(MedicinskaSestra MedicinskaSestra) {
		
		//MedicinskaSestre.getID(JMBG);	
	}
		
	public ArrayList<Pregled> getAllPregledi() throws SQLException {	
		
		Connection conn = Connect.connect();
		
		try {
				PreparedStatement stmt  = conn.prepareStatement("SELECT * FROM  pregled");
				ResultSet rs= stmt.executeQuery(); 
				while(rs.next()) {
					Pregled pre = new Pregled(null, null, null, null, null, null);			
					pre.setPacijent(rs.getString("pacijent"));
					pre.setLekar(rs.getString("lekar"));
					pre.setTermin(rs.getString("termin"));
					pre.setSoba(rs.getString("soba"));
					pre.setOpis(rs.getString("opis"));
					pre.setStatus(rs.getString("status"));
		
					pregledi.add(pre); 
					System.out.print(pre);
			
				}
			}catch (SQLException e) {
	            System.out.println(e.getMessage());
		}
		return pregledi;

		}
	public Pacijent getPacijent(String jmbg) {
		// TODO Auto-generated method stub
		for(Pacijent pacijent : pacijenti) {
			if(pacijent.getJBMG().equals(jmbg)) {
				return pacijent;
			}			
		}
		return null;
	}
	public MedicinskaSestra getMedicinskaSestra(String jmbg) {
		// TODO Auto-generated method stub
		for(MedicinskaSestra medicinskeSestre : medicinskeSestre) {
			if(medicinskeSestre.getJBMG().equals(jmbg)) {
				return medicinskeSestre;
			}			
		}
		return null;
	} 
	public Lekar getLekar(String jmbg) {
		// TODO Auto-generated method stub
		for(Lekar lekar : lekari) {
			if(lekar.getJBMG().equals(jmbg)) {
				return lekar;
			}			
		}
		return null;
	} 
	public Pregled getPreglediLekara(String jmbg) {
		
		for(Pregled pregled : pregledi) {
			if(pregled.getLekar().equals(jmbg)) {
				return pregled;
			}			
		}
		return null;
	} 
	public Pregled getPreglediPacijenta(String jmbg) {
		
		for(Pregled pregled : pregledi) {
			if(pregled.getPacijent().equals(jmbg)) {
				return pregled;
			}			
		}
		return null;
	} 

	
	
}
