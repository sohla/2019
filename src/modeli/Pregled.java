package modeli;

import java.util.Date;

public class Pregled {
	private String pacijent;
	private String lekar;
	private String termin;
	private String soba;
	private String opis;
	private String status;
	
	//public Pregled(String pacijent, String lekar, String termin, String soba, String opis, String status) {
		// TODO Auto-generated constructor stub
//	}
	
	
	public Pregled (String pacijent, String lekar,String termin, String soba, String opis, String status) {
	
			this.pacijent = pacijent;
			this.lekar = lekar;
			this.termin = termin;
			this.soba = soba;
			this.opis = opis;
			this.status = status;
	}
	

	public String getPacijent() {
		return pacijent;
	}
	public void setPacijent(String pacijent) {
		this.pacijent = pacijent;
	}
	public String getLekar() {
		return lekar;
	}
	public void setLekar(String lekar) {
		this.lekar = lekar;
	}
	public String getTermin() {
		return termin;
	}
	public void setTermin(String termin) {
		this.termin = termin;
	}
	public String getSoba() {
		return soba;
	}
	public void setSoba(String soba) {
		this.soba = soba;
	}
	public String getOpis() {
		return opis;	
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
	@Override
	public String toString() {
		return "Pregled [pacijent=" + pacijent + ", lekar=" + lekar + ", termin=" + termin + ", soba=" + soba
				+ ", opis=" + opis + ", status=" + status + "]";
	}

	

}
