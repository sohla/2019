package modeli;

public class MedicinskaSestra extends Osoba {
	
	private Double plata;
	private String sluzba;
	
	public MedicinskaSestra (String ime, String prezime, String JMBG, String pol, String adresa, String brojTelefona, String korisnicko, String lozinka, double plata, String sluzba) {
		
	}
	
	public MedicinskaSestra (Double plata,String sluzba) {
		super();
		this.plata = plata;
		this.sluzba = sluzba;
	}

	public Double getPlata() {
		return plata;
	}

	public void setPlata(Double plata) {
		this.plata = plata;
	}

	public String getSluzba() {
		return sluzba;
	}

	public void setSluzba(String sluzba) {
		this.sluzba = sluzba;
	}



	@Override
	public String toString() {
		return "MedicinskaSestra [plata=" + plata + ", sluzba=" + sluzba + ", ime=" + ime + ", prezime=" + prezime
				+ ", JBMG=" + JBMG + ", pol=" + pol + ", adresa=" + adresa + ", brojTelefona=" + brojTelefona
				+ ", korisnicko=" + korisnicko + ", lozinka=" + lozinka + "]";
	}

	
	

		
	

}
