package modeli;

public class Osoba {
	
	protected String ime;
	protected String prezime;
	protected String JBMG;
	protected String pol;
	protected String adresa;
	protected String brojTelefona;
	protected String korisnicko;
	protected String lozinka;


	public Osoba() {

		}
	
	public Osoba( String ime, String prezime, String JBMG, String pol,String adresa,String brojTelefona, String korisnicko, String lozinka) {

		this.ime = ime;
		this.prezime = prezime;
		this.JBMG = JBMG;
		this.pol = pol;
		this.adresa = adresa;
		this.brojTelefona = brojTelefona;
		this.korisnicko = korisnicko;
		this.lozinka = lozinka;
	
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getJBMG() {
		return JBMG;
	}

	public void setJBMG(String jBMG) {
		JBMG = jBMG;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}

	public String getKorisnicko() {
		return korisnicko;
	}

	public void setKorisnicko(String korisnicko) {
		this.korisnicko = korisnicko;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}


	@Override
	public String toString() {
		return "Osoba [ime=" + ime + ", prezime=" + prezime + ", JBMG=" + JBMG + ", pol=" + pol + ", adresa=" + adresa
				+ ", brojTelefona=" + brojTelefona + ", korisnicko=" + korisnicko + ", lozinka=" + lozinka + "]";
	}

}


