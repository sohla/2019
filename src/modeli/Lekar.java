package modeli;

public class Lekar extends Osoba {
	
	private double plata;
	private String specijalizacija;
	private String sluzba;
	
	public Lekar(String ime, String prezime, String JMBG, String pol, String adresa, String brojTelefona, String korisnicko, String lozinka, Double plata, String specijalizacija, String sluzba) {
		
	}
	
	public Lekar(double plata, String specijalizacija, String sluzba) {
	super();	
	this.plata = plata;
	this.specijalizacija = specijalizacija;
	this.sluzba = sluzba;
		
	}
	
	public double getPlata() {
		return plata;
	}

	public void setPlata(double plata) {
		this.plata = plata;
	}

	public String getSpecijalizacija() {
		return specijalizacija;
	}

	public void setSpecijalizacija(String specijalizacija) {
		this.specijalizacija = specijalizacija;
	}

	public String getSluzba() {
		return sluzba;
	}

	public void setSluzba(String sluzba) {
		this.sluzba = sluzba;
	}

	@Override
	public String toString() {
		return "Lekar [plata=" + plata + ", specijalizacija=" + specijalizacija + ", sluzba=" + sluzba + ", ime=" + ime
				+ ", prezime=" + prezime + ", JBMG=" + JBMG + ", pol=" + pol + ", adresa=" + adresa + ", brojTelefona="
				+ brojTelefona + ", korisnicko=" + korisnicko + ", lozinka=" + lozinka + "]";
	}
/*
	public void setBrojTelefona(int int1) {
		// TODO Auto-generated method stub
		
	}

	public void setSluzba(String string) {
		// TODO Auto-generated method stub
		
	}	
*/
}
