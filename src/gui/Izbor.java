package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import forme.ZakazivanjePregleda;
import gui_izmena_i_dodavanje.LekarProzor;
import gui_izmena_i_dodavanje.MedicinskaSestraProzor;
import gui_izmena_i_dodavanje.PacijentProzor;
import obrada.DAO;

import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Izbor extends JFrame {

	private JFrame frame;
	private JPanel contentPane;
	private JButton Pacijenti;
	private JButton Pregled;
	private DAO dao;

	
	public Izbor(DAO dao) {
	
		this.dao = dao;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize( 400, 400);

		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		
		JButton btnPacijenti = new JButton("Pacijenti");
		btnPacijenti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Izbor.this.dispose();		
				PacijentProzor p = new PacijentProzor(dao);
				p.setVisible(true);
			}
	
		
		});
		btnPacijenti.setBounds(136, 61, 114, 25);
		getContentPane().add(btnPacijenti);
		
		JButton btnMedicinske = new JButton("Medicinske");
		btnMedicinske.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Izbor.this.dispose();		
				MedicinskaSestraProzor m = new MedicinskaSestraProzor(dao);
				m.setVisible(true);
			}
		});
		btnMedicinske.setBounds(136, 119, 114, 25);
		getContentPane().add(btnMedicinske);
		
		JButton bntLekari = new JButton("Lekari");
		bntLekari.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Izbor.this.dispose();		
				LekarProzor l = new LekarProzor(dao);
				l.setVisible(true);
			}
		});
		bntLekari.setBounds(136, 168, 114, 25);
		getContentPane().add(bntLekari);

		JButton bntPregled = new JButton("Pregled");
		bntPregled.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Izbor.this.dispose();		
				ZakazivanjePregleda zp = new ZakazivanjePregleda(dao, null, null);
				zp.setVisible(true);
			}
		});
		bntPregled.setBounds(136, 220, 114, 25);
		getContentPane().add(bntPregled);
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

	}
}
