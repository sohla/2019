package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MedicinskaSestraIzbor extends JFrame {

	
	private JFrame frame;
	private JButton btnPacijenti;
	private JButton btnLekari;
	private JButton btnMedicinske;
	private JButton btnPregled;

	public MedicinskaSestraIzbor() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 400);
		setLocationRelativeTo(null);
		initGUI();
		
		
	}

	private void initGUI() {
	
		frame = new JFrame();
		frame.getContentPane().setLayout(null);

		JButton btnPacijenti = new JButton("Pacijenti");
		btnPacijenti.setBounds(12, 12, 114, 25);
		frame.getContentPane().add(btnPacijenti);
		
		JButton btnLekari = new JButton("Lekari");
		btnLekari.setBounds(159, 12, 114, 25);
		frame.getContentPane().add(btnLekari);
		
		JButton btnMedicinske = new JButton("Medicinske");
		btnMedicinske.setBounds(299, 12, 114, 25);
		frame.getContentPane().add(btnMedicinske);
		
		JButton btnNewButton = new JButton("pregled");
		btnNewButton.setBounds(165, 165, 108, 36);
		frame.getContentPane().add(btnNewButton);
	}
}
