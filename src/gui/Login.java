package gui;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JTextField;

import forme.PacijentPregled;
import gui_izmena_i_dodavanje.PacijentProzor;
import gui_izmena_i_dodavanje.PreglediLekara;
import gui_izmena_i_dodavanje.PreglediPacijenta;
import main.Bolnica;
import modeli.Lekar;
import modeli.MedicinskaSestra;
import modeli.Pacijent;
import obrada.DAO;

import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import net.miginfocom.swing.MigLayout;

public class Login extends JFrame {

	private JLabel lblPozdrav = new JLabel("Dobrodosli. Molimo da se prijavite.");
	private JLabel lblKorisnickoIme = new JLabel("Korisnicko ime");
	private JTextField txtKorisnickoIme = new JTextField(20);
	private JLabel lblSifra = new JLabel("Sifra");
	private JPasswordField txtSifra = new JPasswordField(20);
	private String[] tipovi = {"pacijent", "lekar", "medicinska sestra"};
	private JComboBox tipKorisnika = new JComboBox(tipovi);
	private JButton btnPrijava = new JButton("Prijava");
	private JButton btnOtkazi = new JButton("Otkazi");
	private DAO dao;

	/**
	 * Launch the application.
	 */
	/**
	 * Create the application.
	 */
	public Login(DAO dao) {
		
		this.dao = dao;
		setTitle("Prijava");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		initGUI();
		initListeners();
		pack();
		
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initGUI() {
		

		MigLayout mig = new MigLayout("wrap 2");
		getContentPane().setLayout(mig);
		getContentPane().add(lblPozdrav, "span 2");
		getContentPane().add(lblKorisnickoIme);
		getContentPane().add(txtKorisnickoIme);
		getContentPane().add(lblSifra);
		getContentPane().add(txtSifra);
		getContentPane().add(tipKorisnika);
		getContentPane().add(new JLabel());
		getContentPane().add(btnPrijava, "span 1");
		getContentPane().add(btnOtkazi);
		
		
	}
	
	public void initListeners() {
		btnPrijava.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String korisnickoIme = txtKorisnickoIme.getText().trim();
				String sifra = new String(txtSifra.getPassword()).trim();
				String cmb = ((String) tipKorisnika.getSelectedItem()).trim();
				
				if(cmb=="pacijent")  {
					Pacijent prijava= dao.loginPacijent(korisnickoIme, sifra);
					if (prijava!=null) {
						System.out.println("prijavljena pacijent");
						Login.this.dispose();
						Login.this.setVisible(false);
						
						String jmbg = prijava.getJBMG();
						System.out.println(jmbg); 
						PreglediPacijenta pk = new PreglediPacijenta(dao, jmbg);
						pk.setVisible(true);
					
					}else {
						System.out.println("niko nije prijavljen");
						Login.this.dispose();
						Login.this.setVisible(false);
						
						PacijentProzor pk = new PacijentProzor(dao);
						pk.setVisible(true);
						
					}
				}
					
				else if(cmb=="lekar") {
					Lekar prijava= dao.loginLekar(korisnickoIme, sifra);
					if (prijava!=null) {
						
						System.out.println("prijavljena Lekar");
						Login.this.dispose();
						Login.this.setVisible(false);
						System.out.println("123123123");
						String jmbg = prijava.getJBMG();
						System.out.println(jmbg);
						System.out.println("123123123");
						PreglediLekara pp = new PreglediLekara(dao, jmbg);
						pp.setVisible(true);
					
					}else {
						System.out.println("niko nije prijavljen");
						Login.this.dispose();
						Login.this.setVisible(false);
						
						Login l = new Login(dao);
						l.setVisible(true);
						
					}
				}
				
				
				else if(cmb=="medicinska sestra") {
					MedicinskaSestra prijava= dao.loginMedicinskaSestra(korisnickoIme, sifra);
					if (prijava!=null) {
						System.out.println("prijavljena medicinska sestra");
						Login.this.dispose();
						Login.this.setVisible(false);
						
						Izbor pk = new Izbor(dao);
			     		pk.setVisible(true);
					
					}else {
						System.out.println("niko nije prijavljen");
						Login.this.dispose();
						Login.this.setVisible(false);
						
						Login l = new Login(dao);
						l.setVisible(true);
						
					}
				}
			}
			
		}
				
				
				);
	}
}
