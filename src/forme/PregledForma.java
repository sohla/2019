package forme;

import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import modeli.Pregled;
import net.miginfocom.swing.MigLayout;
import obrada.DAO;

public class PregledForma extends JFrame {

	private JLabel lblLekar = new JLabel("Lekar");
	private JTextField txtLekar = new JTextField(20);
	private JLabel lblPacijent = new JLabel("Pacijent");
	private JTextField txtPacijent = new JTextField(20);
	private JLabel lblTermin = new JLabel("Termin");
	private JTextField txtTermin = new JTextField(20);
	private JLabel lblSoba = new JLabel("Soba");
	private JTextField txtSoba = new JTextField(20);
	private JLabel lblOpis = new JLabel("Opis");
	private JTextField txtOpis = new JTextField(20);
	private JLabel lblStatus= new JLabel("Status");
	private JTextField txtStatus = new JTextField(20);
	
	private JButton btnOk = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	
	private DAO dao;
	private Pregled pregled;


	public PregledForma(DAO dao, Pregled pregled) {
		this.dao = dao;
		this.pregled = pregled;
		GUI();
	}

	
	private void GUI() {
		
		MigLayout layout = new MigLayout("wrap 2");
		setLayout(layout);
		
		if(pregled != null) {
			popuniPolja();
		}
		
		add(lblLekar);
		add(txtLekar);
		add(lblPacijent);
		add(txtPacijent);
		add(lblTermin);
		add(txtTermin);
		add(lblSoba);
		add(txtSoba);
		add(lblOpis);
		add(txtOpis);
		add(lblStatus);
		add(txtStatus);

		add(new JLabel());
		add(btnOk, "split 2");
		add(btnCancel);
		
	}

	private void popuniPolja() {
		
		txtLekar.setText(pregled.getLekar());
		txtPacijent.setText(pregled.getPacijent());
		txtTermin.setText(pregled.getTermin());
		txtSoba.setText(pregled.getSoba());
		txtOpis.setText(pregled.getOpis());
		txtStatus.setText(pregled.getStatus());

		
	}

}
