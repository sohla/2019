package forme;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import modeli.MedicinskaSestra;
import modeli.Pacijent;
import net.miginfocom.swing.MigLayout;
import obrada.DAO;

public class MedicinskaSestraForma extends JFrame {
	
	private JLabel lblIme = new JLabel("Ime");
	private JTextField txtIme = new JTextField(20);
	private JLabel lblPrezime = new JLabel("Prezime");
	private JTextField txtPrezime = new JTextField(20);
	private JLabel lblJMBG = new JLabel("JMBG");
	private JTextField txtJMBG = new JTextField(20);
	private JLabel lblPol = new JLabel("Pol");
	private JTextField txtPol = new JTextField(20);
	private JLabel lblAdresa = new JLabel("Adresa");
	private JTextField txtAdresa = new JTextField(20);
	private JLabel lblbrojTelefona = new JLabel("Broj telefona");
	private JTextField txtBrojTelefona = new JTextField(20);
	private JLabel lblkorisnicko = new JLabel("Korisnicko");
	private JTextField txtKorisnicko = new JTextField(20);
	private JLabel lblLozinka = new JLabel("Lozinka");
	private JTextField txtLozinka = new JTextField(20);
	
	private JLabel lblPlata = new JLabel("Plata");
	private JTextField txtPlata = new JTextField(20);
	private JLabel lblSluzba = new JLabel("Sluzba");
	private JTextField txtSluzba = new JTextField(20);

	private JButton btnOk = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	
	private DAO dao;
	private MedicinskaSestra medicinska;
	

	public MedicinskaSestraForma(DAO dao, MedicinskaSestra medicinska) {
		this.dao = dao;
		this.medicinska = medicinska;
		initialize();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pack();
		setResizable(false);
	}


	private void initialize() {
		MigLayout layout = new MigLayout("wrap 2");
		setLayout(layout);
		
		if(medicinska != null) {
			popuniPolja();
		}
		
		add(lblIme);
		add(txtIme);
		add(lblPrezime);
		add(txtPrezime);
		add(lblJMBG);
		add(txtJMBG);
		add(lblPol);
		add(txtPol);
		add(lblAdresa);
		add(txtAdresa);
		add(lblbrojTelefona);
		add(txtBrojTelefona);
		add(lblkorisnicko);
		add(txtKorisnicko);
		add(lblLozinka);
		add(txtLozinka);
		add(lblPlata);
		add(txtPlata);
		add(lblSluzba);
		add(txtSluzba);
		
		add(new JLabel());
		add(btnOk, "split 2");
		add(btnCancel);

	}
	
	private void popuniPolja() {
	
		txtIme.setText(medicinska.getIme());	
		txtPrezime.setText(medicinska.getPrezime());
		txtJMBG.setText(medicinska.getJBMG());
		txtPol.setText(medicinska.getPol());
		txtAdresa.setText(medicinska.getAdresa());
		txtBrojTelefona.setText(medicinska.getBrojTelefona());
		txtKorisnicko.setText(medicinska.getKorisnicko());
		txtLozinka.setText(medicinska.getLozinka());
		txtPlata.setText(String.valueOf(medicinska.getPlata()));
		txtSluzba.setText(String.valueOf(medicinska.getSluzba()));
		
	}


	private void initActions() {
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(validacija() == true){
					String ime = txtIme.getText().trim();
					String prezime = txtPrezime.getText().trim();
					String JMBG = txtJMBG.getText().trim();
					String pol = txtPol.getText().trim();
					String adresa = txtAdresa.getText().trim();
					String brojTelefona = txtAdresa.getText().trim();
					String korisnicko = txtKorisnicko.getText().trim();
					String lozinka = txtLozinka.getText().trim();
					
					Double plata = Double.parseDouble(txtPlata.getText().trim());
					String sluzba  = txtSluzba.getText().trim();

					if(medicinska == null) {
						medicinska = new MedicinskaSestra(ime,prezime,JMBG,pol,adresa,brojTelefona,korisnicko,lozinka,plata,sluzba);
						dao.getMedinickeSestre().add(medicinska);
					}
					
				}
			}
		});
	}
	
	private boolean validacija() {
	
		boolean ok = true;
		String poruka = "unesite ispravne podatke :";
		if(txtIme.getText().trim().equals("")) {
			poruka += "- Unesite ime\n";
			ok = false;
		}
		if(txtPrezime.getText().trim().equals("")) {
			poruka += "- Unesite prezime\n";
			ok = false;
		}
		if(txtJMBG.getText().trim().equals("")) {
			poruka += "- Unesite JMBG\n";
			ok = false;
		}
		if(txtPol.getText().trim().equals("")) {
			poruka += "- Unesite pol\n";
			ok = false;
		}
		if(txtAdresa.getText().trim().equals("")) {
			poruka += "- Unesite adresa\n";
			ok = false;
		}
		
		if(txtBrojTelefona.getText().trim().equals("")) {
			poruka += "- Unesite broj telefona\n";
			ok = false;
		}
		
		if(txtKorisnicko.getText().trim().equals("")) {
			poruka += "- Unesite korisnicko ime\n";
			ok = false;
		}
		if(txtLozinka.getText().trim().equals("")) {
			poruka += "- Unesite lozinke\n";
			ok = false;
		}

		if(txtPlata.getText().trim().equals("")) {
			poruka += "- Unesite platu\n";
			ok = false;
		}
		if(txtSluzba.getText().trim().equals("")) {
			poruka += "- Unesite lozinku\n";
			ok = false;
		}
		if(ok == false) {
			JOptionPane.showMessageDialog(null, poruka, "Neispravni podaci", JOptionPane.WARNING_MESSAGE);
		}
		return ok;
		
		
	}

}
