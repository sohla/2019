package forme;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import modeli.Lekar;
import modeli.Pregled;
import net.miginfocom.swing.MigLayout;
import obrada.DAO;

public class PacijentPregled extends JFrame {

	private JFrame frame;
	private JLabel lblText = new JLabel("unos opisa");
	private JTextField txtText = new JTextField(35);
	private JButton btnOk = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	private DAO dao;
	private Pregled pregled;


	public PacijentPregled(DAO dao, Pregled pregled) {
		//set
		//initialize();
		this.dao = dao;
		this.pregled = pregled;
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GUI();
		pack();
		setResizable(false);
		GUI();
	}

	private void GUI() {
		MigLayout layout = new MigLayout("wrap 1");
		setLayout(layout);
		add(lblText);
		add(txtText);
		add(btnOk);
		add(btnCancel);
		add(btnOk, "split 2");
		add(btnCancel);
	
	}
	
	private void initActions() {
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(validacija() == true) {

					String opis = txtText.getText().trim();
					if(pregled == null) {
						pregled = new Pregled(null,null,null,null,opis,null);
						dao.getPregledi().add(pregled);
					}
					
				}
			}


		});
	}
	
	private boolean validacija() {
		// TODO Auto-generated method stub
		boolean ok = true;
		String poruka = "unesite ";
		if(txtText.getText().trim().equals("")) {
			poruka += "opis";
			ok = false;
		}
		if(ok == false) {
			JOptionPane.showMessageDialog(null, poruka, "Neispravni podaci", JOptionPane.WARNING_MESSAGE);
		}
		return ok;
	}


}
