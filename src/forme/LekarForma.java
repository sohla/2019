package forme;

import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


import modeli.Lekar;
import modeli.Sluzba;
import net.miginfocom.swing.MigLayout;
import obrada.DAO;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LekarForma extends JFrame {

	private JLabel lblIme = new JLabel("Ime");
	private JTextField txtIme = new JTextField(20);
	private JLabel lblPrezime = new JLabel("Prezime");
	private JTextField txtPrezime = new JTextField(20);
	private JLabel lblJMBG = new JLabel("JMBG");
	private JTextField txtJMBG = new JTextField(20);
	private JLabel lblPol = new JLabel("Pol");
	private JTextField txtPol = new JTextField(20);
	private JLabel lblAdresa = new JLabel("Adresa");
	private JTextField txtAdresa = new JTextField(20);
	private JLabel lblbrojTelefona = new JLabel("Broj telefona");
	private JTextField txtBrojTelefona = new JTextField(20);
	private JLabel lblkorisnicko = new JLabel("Korisnicko");
	private JTextField txtKorisnicko = new JTextField(20);
	private JLabel lblLozinka = new JLabel("Lozinka");
	private JTextField txtLozinka = new JTextField(20);
	
	private JLabel lblPlata = new JLabel("Plata");
	private JTextField txtPlata = new JTextField(20);
	private JLabel lblSpecijalizacija= new JLabel("Specijalizacija");
	private JTextField txtSpecijalizacija = new JTextField(20);
	private JLabel lblSluzba = new JLabel("Sluzba");
	private JTextField txtSluzba = new JTextField(20);
	
	private JButton btnOk = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	
	private DAO dao;
	private Lekar lekar;

	/**
	 * Create the application.
	 */
	public LekarForma(DAO dao,Lekar lekar) {
		this.dao = dao;
		this.lekar = lekar;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GUI();
		pack();
		setResizable(false);
		initActions();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void GUI() {
		MigLayout layout = new MigLayout("wrap 2");
		getContentPane().setLayout(layout);
		
		if(lekar!=null) {
			popuniPolja();
		}
		
		getContentPane().add(lblIme);
		getContentPane().add(txtIme);
		getContentPane().add(lblPrezime);
		getContentPane().add(txtPrezime);
		getContentPane().add(lblJMBG);
		getContentPane().add(txtJMBG);
		getContentPane().add(lblPol);
		getContentPane().add(txtPol);
		getContentPane().add(lblAdresa);
		getContentPane().add(txtAdresa);
		getContentPane().add(lblbrojTelefona);
		getContentPane().add(txtBrojTelefona);
		getContentPane().add(lblkorisnicko);
		getContentPane().add(txtKorisnicko);
		getContentPane().add(lblLozinka);
		getContentPane().add(txtLozinka);
		
		getContentPane().add(lblPlata);
		getContentPane().add(txtPlata);
		getContentPane().add(lblSpecijalizacija);
		getContentPane().add(txtSpecijalizacija);
		
		getContentPane().add(lblSluzba);
		getContentPane().add(txtSluzba);
		
		
		getContentPane().add(new JLabel());
		getContentPane().add(btnOk, "split 2");
		getContentPane().add(btnCancel);
	}
	
	
	private void popuniPolja() {
		
		txtIme.setText(lekar.getIme());	
		txtPrezime.setText(lekar.getPrezime());
		txtJMBG.setText(lekar.getJBMG());
		txtPol.setText(lekar.getPol());
		txtAdresa.setText(lekar.getAdresa());
		txtBrojTelefona.setText(lekar.getBrojTelefona());
		txtKorisnicko.setText(lekar.getKorisnicko());
		txtLozinka.setText(lekar.getLozinka());
		txtPlata.setText(String.valueOf(lekar.getPlata()));
		txtSpecijalizacija.setText(lekar.getSpecijalizacija());
		txtSluzba.setText(lekar.getSluzba().toString()); //pacijent.getBroj().toString()

		
	}

	private void initActions() {
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(validacija() == true) {
					String ime = txtIme.getText().trim();
					String prezime = txtPrezime.getText().trim();
					String JMBG = txtJMBG.getText().trim();
					String pol = txtPol.getText().trim();
					String adresa = txtAdresa.getText().trim();
					String brojTelefona = txtAdresa.getText().trim();
					String korisnicko = txtKorisnicko.getText().trim();
					String lozinka = txtLozinka.getText().trim();
					Double plata = Double.parseDouble(txtPlata.getText().trim());
					String specijalizacija = txtSpecijalizacija.getText().trim();
					String sluzba = txtSluzba.getText().trim();
					if(lekar == null) {
						lekar = new Lekar(ime,prezime,JMBG,pol,adresa,brojTelefona,korisnicko,lozinka,plata,specijalizacija,sluzba);
						dao.getLekari().add(lekar);
					}
					
				}
			}
		});
	}

	public boolean validacija() {
		
		boolean ok = true;
		String poruka = "unesite ispravne podatke :";
		if(txtIme.getText().trim().equals("")) {
			poruka += "- Unesite ime\n";
			ok = false;
		}
		if(txtPrezime.getText().trim().equals("")) {
			poruka += "- Unesite prezime\n";
			ok = false;
		}
		if(txtJMBG.getText().trim().equals("")) {
			poruka += "- Unesite JMBG\n";
			ok = false;
		}
		if(txtPol.getText().trim().equals("")) {
			poruka += "- Unesite pol\n";
			ok = false;
		}
		if(txtAdresa.getText().trim().equals("")) {
			poruka += "- Unesite adresa\n";
			ok = false;
		}
		
		if(txtBrojTelefona.getText().trim().equals("")) {
			poruka += "- Unesite broj telefona\n";
			ok = false;
		}
		
		if(txtKorisnicko.getText().trim().equals("")) {
			poruka += "- Unesite korisnicko ime\n";
			ok = false;
		}
		if(txtLozinka.getText().trim().equals("")) {
			poruka += "- Unesite lozinke\n";
			ok = false;
		}

		if(txtPlata.getText().trim().equals("")) {
			poruka += "- Unesite platu\n";
			ok = false;
		}
		
		if(txtSpecijalizacija.getText().trim().equals("")) {
			poruka += "- Unesite specijalizacija\n";
			ok = false;
		}

		if(txtSluzba.getText().trim().equals("")) {
			poruka += "- Unesite sluzba\n";
			ok = false;
		}
		if(ok == false) {
			JOptionPane.showMessageDialog(null, poruka, "Neispravni podaci", JOptionPane.WARNING_MESSAGE);
		}
		return ok;
		
	}
	
}

/*
 * 		add(lblImePacijenta);
	add(txtImePacijenta);
	add(lblPrezimePacijenta);
	add(txtPrezimePacijenta);
	add(lblJMBGPacijenta);
	add(txtJMBGPacijenta);
	
	add(lblImeLekara);
	add(txtImeLekara);
	add(lblPrezimeLekara);
	add(txtPrezimeLekara);
	add(lblJMBGLekara);
	add(txtJMBGLekara);
	
	add(lblDatum);
	add(txtDatum);
	add(lblSoba);
	add(txtSoba);
	add(lblOpis);
	add(txtOpis);
	add(lblStatus);
	add(txtStatus);
 */
