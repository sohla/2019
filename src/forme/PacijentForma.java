package forme;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import modeli.Pacijent;
import net.miginfocom.swing.MigLayout;
import obrada.DAO;

public class PacijentForma extends JFrame {

	private JLabel lblIme = new JLabel("Ime");
	private JTextField txtIme = new JTextField(20);
	private JLabel lblPrezime = new JLabel("Prezime");
	private JTextField txtPrezime = new JTextField(20);
	private JLabel lblJMBG = new JLabel("JMBG");
	private JTextField txtJMBG = new JTextField(20);
	private JLabel lblPol = new JLabel("Pol");
	private JTextField txtPol = new JTextField(20);
	private JLabel lblAdresa = new JLabel("Adresa");
	private JTextField txtAdresa = new JTextField(20);
	private JLabel lblbrojTelefona = new JLabel("Broj telefona");
	private JTextField txtBrojTelefona = new JTextField(20);
	private JLabel lblkorisnicko = new JLabel("Korisnicko");
	private JTextField txtKorisnicko = new JTextField(20);
	private JLabel lblLozinka = new JLabel("Lozinka");
	private JTextField txtLozinka = new JTextField(20);
	
	private JLabel lblLekar = new JLabel("Lekar");
	private JTextField txtLekar = new JTextField(20);
	private JLabel lblBroj = new JLabel("Broj");
	private JTextField txtBroj = new JTextField(20);
	private JLabel lblDatumIsteka = new JLabel("Datum Isteke");
	private JTextField txtDatumIsteka = new JTextField(20);
	private JLabel lblKategorija = new JLabel("Kategorija");
	private JTextField txtKategorija = new JTextField(20);
	
	private JButton btnOk = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	
	private DAO dao;
	private Pacijent pacijent;
	
	public PacijentForma(DAO dao,Pacijent pacijent) {
		this.dao = dao;
		this.pacijent = pacijent;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GUI();
		pack();
		setResizable(false);
		initActions();
	}

	private void GUI() {
		MigLayout layout = new MigLayout("wrap 2");
		setLayout(layout);
		
		if(pacijent != null) {
			popuniPolja();
		}
		
		add(lblIme);
		add(txtIme);
		add(lblPrezime);
		add(txtPrezime);
		add(lblJMBG);
		add(txtJMBG);
		add(lblPol);
		add(txtPol);
		add(lblAdresa);
		add(txtAdresa);
		add(lblbrojTelefona);
		add(txtBrojTelefona);
		add(lblkorisnicko);
		add(txtKorisnicko);
		add(lblLozinka);
		add(txtLozinka);
		add(lblLekar);
		add(txtLekar);
		add(lblBroj);
		add(txtBroj);
		add(lblDatumIsteka);
		add(txtDatumIsteka);
		add(lblKategorija);
		add(txtKategorija);
		
		add(new JLabel());
		add(btnOk, "split 2");
		add(btnCancel);
		
	}
	
	private void initActions() {
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(validacija() == true) {
					String ime = txtIme.getText().trim();
					String prezime = txtPrezime.getText().trim();
					String JMBG = txtJMBG.getText().trim();
					String pol = txtPol.getText().trim();
					String adresa = txtAdresa.getText().trim();
					String brojTelefona = txtAdresa.getText().trim();
					String korisnicko = txtKorisnicko.getText().trim();
					String lozinka = txtLozinka.getText().trim();
					String lekar = txtLekar.getText().trim();
					int broj = Integer.parseInt(txtBroj.getText().trim());
					String datumIsteka = txtDatumIsteka.getText().trim();
					String kategorija = txtKategorija.getText().trim();

					if(pacijent == null) {
						pacijent = new Pacijent(ime,prezime,JMBG,pol,adresa,brojTelefona,korisnicko,lozinka,lekar,broj,datumIsteka,kategorija);
						dao.getPacijenti().add(pacijent);
						System.out.print(pacijent);
					}
					
					PacijentForma.this.dispose();
					PacijentForma.this.setVisible(true);
					
				}
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				PacijentForma.this.dispose();
				
			}
			
		});
	}
	
	public void popuniPolja() {
		
		txtIme.setText(pacijent.getIme());
		txtPrezime.setText(pacijent.getPrezime());
		txtJMBG.setText(pacijent.getJBMG());
		txtPol.setText(pacijent.getPol());
		txtAdresa.setText(pacijent.getAdresa());
		txtBrojTelefona.setText(pacijent.getBrojTelefona());
		txtKorisnicko.setText(pacijent.getKorisnicko());
		txtLozinka.setText(pacijent.getLozinka());
		txtLekar.setText(pacijent.getLekar());
		txtBroj.setText(String.valueOf(pacijent.getBroj())); //pacijent.getBroj().toString()
		txtDatumIsteka.setText(pacijent.getDatumIsteka().toString());
		txtKategorija.setText(String.valueOf(pacijent.getKategorija()));//pacijent.getKategorija().toString()
		
	}
	
	private boolean validacija() {
		
		boolean ok = true;
		String poruka = "unesite ispravne podatke :\n";
		if(txtIme.getText().trim().equals("")) {
			poruka += "- Unesite ime\n";
			ok = false;
		}
		if(txtPrezime.getText().trim().equals("")) {
			poruka += "- Unesite prezime\n";
			ok = false;
		}
		if(txtJMBG.getText().trim().equals("")) {
			poruka += "- Unesite JMBG\n";
			ok = false;
		}
		if(txtPol.getText().trim().equals("")) {
			poruka += "- Unesite pol\n";
			ok = false;
		}
		if(txtAdresa.getText().trim().equals("")) {
			poruka += "- Unesite adresa\n";
			ok = false;
		}
		
		if(txtBrojTelefona.getText().trim().equals("")) {
			poruka += "- Unesite broj telefona\n";
			ok = false;
		}
		
		if(txtKorisnicko.getText().trim().equals("")) {
			poruka += "- Unesite korisnicko ime\n";
			ok = false;
		}
		if(txtLozinka.getText().trim().equals("")) {
			poruka += "- Unesite lozinke\n";
			ok = false;
		}
		if(txtLekar.getText().trim().equals("")) {
			poruka += "- Unesite lekara\n";
			ok = false;
		}
		if(txtBroj.getText().trim().equals("")) {
			poruka += "- Unesite bojr\n";
			ok = false;
		}
		if(txtDatumIsteka.getText().trim().equals("")) {
			poruka += "- Unesite datum isteka\n";
			ok = false;
		}
		if(txtKategorija.getText().trim().equals("")) {
			poruka += "- Unesite kategoriju\n";
			ok = false;
		}
		if(ok == false) {
			JOptionPane.showMessageDialog(null, poruka, "Neispravni podaci", JOptionPane.WARNING_MESSAGE);
		}
		return ok;
		
	}

}
