package forme;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import gui_izmena_i_dodavanje.LekarProzor;
import gui_izmena_i_dodavanje.PacijentProzor;
import modeli.Lekar;
import modeli.Pacijent;
import net.miginfocom.swing.MigLayout;
import obrada.DAO;

public class ZakazivanjePregleda extends JFrame {
	
	private JPanel panel;
	
	private JButton btnPacijent = new JButton("Pacijent");
	private JButton btnLekar = new JButton("Lekar");

	private JLabel lblImePacijenta = new JLabel("Ime pacijenta");
	private JTextField txtImePacijenta  = new JTextField(20);
	private JLabel lblPrezimePacijenta  = new JLabel("Prezime pacijenta");
	private JTextField txtPrezimePacijenta  = new JTextField(20);
	private JLabel lblJMBGPacijenta  = new JLabel("JMBG pacijenta");
	private JTextField txtJMBGPacijenta  = new JTextField(20);

	private JLabel lblImeLekara = new JLabel("Ime Lekara");
	private JTextField txtImeLekara  = new JTextField(20);
	private JLabel lblPrezimeLekara  = new JLabel("Prezime Lekara");
	private JTextField txtPrezimeLekara  = new JTextField(20);
	private JLabel lblJMBGLekara  = new JLabel("JMBG Lekara");
	private JTextField txtJMBGLekara  = new JTextField(20);
	
	private JLabel lblDatum = new JLabel("Datum");
	private JTextField txtDatum = new JTextField(20);
	private JLabel lblSoba = new JLabel("Soba");
	private JTextField txtSoba = new JTextField(20);
	private JLabel lblOpis = new JLabel("Opis");
	private JTextField txtOpis = new JTextField(20);
	private JLabel lblStatus = new JLabel("Status");
	private JTextField txtStatus = new JTextField(20);
	
	private JButton btnOk = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	private JButton btnDodajLekara = new JButton("Dodaj Lekara");
	private JButton btnDodajPacijenta = new JButton("Dodaj Pacijent");
	
	private DAO dao;
	private Lekar lekar;
	private Pacijent pacijent;
	
	private LekarProzor lekari;
	private PacijentProzor pacijenti;
	private JButton btnPregled = new JButton();
	private JToolBar mainToolbar = new JToolBar();
	private DefaultTableModel tableModel;;
	private JTable lekariTabela;
	private JTable pacijentiTabela;
	

	public ZakazivanjePregleda(DAO dao,Pacijent pacijent ,Lekar lekart) {
		this.dao = dao;
		this.lekar = lekar;
		this.pacijent = pacijent;
		//this.zk = zk;
		setLocationRelativeTo(null);
		initialize();
		initActions();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setResizable(true);
	}

	
	private void initialize() {
		
		if(pacijent!=null) {
			popuniPoljaPacijent(pacijent);
		}

		if(lekar!=null) {
			popuniPoljaLekar(lekar);
		}
		
		MigLayout layout = new MigLayout("wrap 2");
		setLayout(layout);
		/*
		add(new JLabel());
		add(btnPacijent, "split 2");
		add(btnLekar);
		*/
		add(new JLabel(), "split 2");
		add(lblImePacijenta);
		add(txtImePacijenta);
		add(lblPrezimePacijenta);
		add(txtPrezimePacijenta);
		add(lblJMBGPacijenta);
		add(txtJMBGPacijenta);
		
		add(lblImeLekara);
		add(txtImeLekara);
		add(lblPrezimeLekara);
		add(txtPrezimeLekara);
		add(lblJMBGLekara);
		add(txtJMBGLekara);
		
		add(lblDatum);
		add(txtDatum);
		add(lblSoba);
		add(txtSoba);
		add(lblOpis);
		add(txtOpis);
		add(lblStatus);
		add(txtStatus);
		
		add(new JLabel());
		add(btnOk, "split 2");
		add(btnCancel);
		add(btnDodajLekara);
		add(btnDodajPacijenta);
		
		
		int brojLekara = dao.getLekari().size();
		String[] zaglavlje = new String[] {"ime","prezime","JMBG","pol","adresa","brojtelefona","korisnicko","lozinka","plata","sluzba","specijalizacija"};
		Object[][] podaci = new Object[brojLekara][zaglavlje.length];{
		
		for(int i=0; i<dao.getLekari().size(); i++) {
			Lekar lekar = dao.getLekari().get(i);
			podaci[i][0] = lekar.getIme();
			podaci[i][1] = lekar.getPrezime();
			podaci[i][2] = lekar.getJBMG();
			podaci[i][3] = lekar.getPol();
			podaci[i][4] = lekar.getAdresa();
			podaci[i][5] = lekar.getBrojTelefona();			
			podaci[i][6] = lekar.getKorisnicko();
			podaci[i][7] = lekar.getLozinka();
			podaci[i][8] = lekar.getPlata();
			podaci[i][9] = lekar.getSpecijalizacija();	
			podaci[i][10] = lekar.getSluzba();

			
		}
		tableModel = new DefaultTableModel(podaci,zaglavlje);
		lekariTabela = new JTable(tableModel);
		lekariTabela.setRowSelectionAllowed(true);
		lekariTabela.setColumnSelectionAllowed(false);
		lekariTabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lekariTabela.setDefaultEditor(Object.class, null);

	}
		
	int brojPacijenata = dao.getPacijenti().size();
	String[] zaglavljeP = new String[] {"ime","prezime","JMBG","pol","adresa","brojtelefona","korisnicko","lozinka","lekar","broj","datum isteka","kategorija"};
	Object[][] podaciP = new Object[brojPacijenata][zaglavlje.length];
		
	for(int i=0; i<dao.getPacijenti().size(); i++) {
		Pacijent pacijent = dao.getPacijenti().get(i);
		podaci[i][0] = pacijent.getIme();
		podaci[i][1] = pacijent.getPrezime();
		podaci[i][2] = pacijent.getJBMG();
		podaci[i][3] = pacijent.getPol();
		podaci[i][4] = pacijent.getAdresa();
		podaci[i][5] = pacijent.getBrojTelefona();			
		podaci[i][6] = pacijent.getKorisnicko();
		podaci[i][7] = pacijent.getLozinka();
		podaci[i][8] = pacijent.getLekar();
		podaci[i][9] = pacijent.getBroj();	
		podaci[i][10] = pacijent.getDatumIsteka();
		//podaci[i][11] = pacijent.getKategorija();
			
	}
		
		tableModel = new DefaultTableModel(podaci,zaglavlje);
		pacijentiTabela = new JTable(tableModel);
		pacijentiTabela.setRowSelectionAllowed(true);
		pacijentiTabela.setColumnSelectionAllowed(false);
		pacijentiTabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		pacijentiTabela.setDefaultEditor(Object.class, null);
			
		JScrollPane scrollPane = new JScrollPane(pacijentiTabela);
		add(scrollPane, BorderLayout.CENTER);
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.addTab("Lekari", lekariTabela);
		tabbedPane.addTab("Pacijenti", pacijentiTabela);
		tabbedPane.setSize(500,500);
		add(tabbedPane, "east");


}
	private void popuniPoljaLekar(Lekar lekar) {
	
		txtImeLekara.setText(lekar.getIme());
		txtPrezimeLekara.setText(lekar.getPrezime());
		txtJMBGLekara.setText(lekar.getJBMG());
	
	}
	
	private void popuniPoljaPacijent(Pacijent pacijent) {
		
		txtImePacijenta.setText(pacijent.getIme());
		txtPrezimePacijenta.setText(pacijent.getPrezime());
		txtJMBGPacijenta.setText(pacijent.getJBMG());

	
}
	private void initActions() {
		btnLekar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Izbor.this.dispose();		
				LekarProzor l = new LekarProzor(dao);
				l.setVisible(true);
			}
		});
		btnPacijent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Izbor.this.dispose();		
				PacijentProzor l = new PacijentProzor(dao);
				l.setVisible(true);
			}
		});
		
		
		
		btnDodajLekara.addActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int red = lekariTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati red");	
				}else {
					String jmbg = lekariTabela.getValueAt(red, 2).toString();
					Lekar lekar = dao.getLekar(jmbg);
					if(lekar!=null) {
						popuniPoljaLekar(lekar);
					}else {
						JOptionPane.showMessageDialog(null, "greska");
					}
				}
			}
			
		});
		btnDodajPacijenta.addActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int red = pacijentiTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati red");	
				}else {
					String jmbg = pacijentiTabela.getValueAt(red, 2).toString();
					Pacijent pacijent = dao.getPacijent(jmbg);
					if(pacijent!=null) {
						popuniPoljaPacijent(pacijent);
					}else {
						JOptionPane.showMessageDialog(null, "greska");
					}
				}
			}
			
		});
		
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(validacija() == true) {
					
				}
			}
		});
	
		}
	
	
	public boolean validacija() {
		
		boolean ok = true;
		String poruka = "unesite ispravne podatke :";
		if(txtImePacijenta.getText().trim().equals("")) {
			poruka += "- Unesite ime pacijenta \n";
			ok = false;
		}
		if(txtPrezimePacijenta.getText().trim().equals("")) {
			poruka += "- Unesite prezime pacijenta\n";
			ok = false;
		}
		if(txtJMBGPacijenta.getText().trim().equals("")) {
			poruka += "- Unesite JMBG pacijenta \n";
			ok = false;
		}
		if(txtImeLekara.getText().trim().equals("")) {
			poruka += "- Unesite ime lekara\n";
			ok = false;
		}
		if(txtPrezimeLekara.getText().trim().equals("")) {
			poruka += "- Unesite prezime lekara\n";
			ok = false;
		}
		
		if(txtJMBGLekara.getText().trim().equals("")) {
			poruka += "- Unesite JMBG lekara\n";
			ok = false;
		}
		
		if(txtDatum.getText().trim().equals("")) {
			poruka += "- Unesite datum pregleda\n";
			ok = false;
		}
		if(txtSoba.getText().trim().equals("")) {
			poruka += "- Unesite sobu pregleda\n";
			ok = false;
		}

		if(txtOpis.getText().trim().equals("")) {
			poruka += "- Unesite opis pregleda\n";
			ok = false;
		}
		
		if(txtStatus.getText().trim().equals("")) {
			poruka += "- Unesite status pregleda\n";
			ok = false;
		}
		if(ok == false) {
			JOptionPane.showMessageDialog(null, poruka, "Neispravni podaci", JOptionPane.WARNING_MESSAGE);
		}
		return ok;
		
	}
	
	
}	
