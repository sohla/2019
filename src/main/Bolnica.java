package main;
import java.sql.SQLException;

import gui.Login;
import obrada.DAO;


public class Bolnica {
	
	public static void main(String[] args) throws SQLException {
			
		DAO dao = new DAO();
		dao.getAllMedicinskaSestra();
		dao.getAllLekar();
		dao.getAllPacijent();
		dao.getAllPregledi();
		System.out.println("bolnica get all");
		Login li = new Login(dao);
		System.out.println("log in");
		li.setVisible(true);
	}

}
