package gui_izmena_i_dodavanje;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import forme.LekarForma;
import forme.MedicinskaSestraForma;
import modeli.Lekar;
import modeli.MedicinskaSestra;
import modeli.Pacijent;
import obrada.DAO;

public class MedicinskaSestraProzor extends JFrame {

	private JToolBar mainToolbar = new JToolBar();
	private JButton btnAdd = new JButton();
	private JButton btnEdit = new JButton();
	private JButton btnDelete = new JButton();

	private DefaultTableModel tableModel;
	private JTable medicinskeTabela;
	
	private DAO dao;
	
	public MedicinskaSestraProzor(DAO dao) {
		this.dao = dao;
		setTitle("Lekar");
		setSize(800, 300);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		initActions();
		GUI();
		
	}


	private void GUI() {
		ImageIcon addIcon = new ImageIcon(getClass().getResource("/slike/add.gif"));
		btnAdd.setIcon(addIcon);
		mainToolbar.add(btnAdd);
		ImageIcon editIcon = new ImageIcon(getClass().getResource("/slike/edit.gif"));
		btnEdit.setIcon(editIcon);
		mainToolbar.add(btnEdit);
		ImageIcon deleteIcon = new ImageIcon(getClass().getResource("/slike/remove.gif"));
		btnDelete.setIcon(deleteIcon);
		mainToolbar.add(btnDelete);
		add(mainToolbar, BorderLayout.NORTH);
		
		int brojMedicinskaSestra = dao.getPacijenti().size();
		String[] zaglavlje = new String[] {"ime","prezime","JMBG","pol","adresa","brojtelefona","korisnicko","lozinka","plata","sluzba","specijalizacija"};
		Object[][] podaci = new Object[brojMedicinskaSestra][zaglavlje.length];
		
		for(int i=0; i<dao.getMedinickeSestre().size(); i++) {
			MedicinskaSestra medicinska = dao.getMedinickeSestre().get(i);
			podaci[i][0] = medicinska.getIme();
			podaci[i][1] = medicinska.getPrezime();
			podaci[i][2] = medicinska.getJBMG();
			podaci[i][3] = medicinska.getPol();
			podaci[i][4] = medicinska.getAdresa();
			podaci[i][5] = medicinska.getBrojTelefona();			
			podaci[i][6] = medicinska.getKorisnicko();
			podaci[i][7] = medicinska.getLozinka();	
			podaci[i][8] = medicinska.getPlata();	
			podaci[i][10] = medicinska.getSluzba();

			
		}
		tableModel = new DefaultTableModel(podaci,zaglavlje);
		medicinskeTabela = new JTable(tableModel);
		medicinskeTabela.setRowSelectionAllowed(true);
		medicinskeTabela.setColumnSelectionAllowed(false);
		medicinskeTabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		medicinskeTabela.setDefaultEditor(Object.class, null);
		
		JScrollPane scrollPane = new JScrollPane(medicinskeTabela);
		add(scrollPane, BorderLayout.CENTER);
	}
	
	public void initActions() {
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MedicinskaSestraForma lf = new MedicinskaSestraForma(dao, null);
				lf.setVisible(true);

			}
			
		});
		btnEdit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int red = medicinskeTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "morate izabrati red");
				}else {
					String jmbg = medicinskeTabela.getValueAt(red, 2).toString();
					MedicinskaSestra medicinska = dao.getMedicinskaSestra(jmbg); 
					if(medicinska != null) {
						MedicinskaSestraForma mf = new MedicinskaSestraForma(dao,medicinska);
						mf.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "greska");
					}
				}
				
			}
			
			
		});
		btnDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int red = medicinskeTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati red");
				}else {
					String jmbg = medicinskeTabela.getValueAt(red, 2).toString();
					MedicinskaSestra sestra = dao.getMedicinskaSestra(jmbg);
					if(sestra != null) {
						int izbor = JOptionPane.showConfirmDialog(null, "Da li ste sigurni da zelite da izbrisete pacijenta", sestra.getJBMG(),JOptionPane.YES_NO_OPTION);
						
						if(izbor == JOptionPane.YES_OPTION) {
							
							DefaultTableModel model = (DefaultTableModel) medicinskeTabela.getModel();
							dao.getMedinickeSestre().remove(sestra);
							model.removeRow(red);
							
						}
					}else {
						JOptionPane.showMessageDialog(null, "greska");
					}
				}
				
			}
			
		});
	}

}
