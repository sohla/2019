package gui_izmena_i_dodavanje;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import forme.LekarForma;
import forme.ZakazivanjePregleda;
import modeli.Lekar;
import modeli.MedicinskaSestra;
import modeli.Pacijent;
import obrada.DAO;

public class LekarProzor extends JFrame {

	private JToolBar mainToolbar = new JToolBar();
	private JButton btnAdd = new JButton();
	private JButton btnEdit = new JButton();
	private JButton btnDelete = new JButton();
	private JButton btnPregled = new JButton();

	private DefaultTableModel tableModel;
	private JTable lekariTabela;
	
	private DAO dao;
	private ZakazivanjePregleda zk;
	
	public LekarProzor(DAO dao) {
		
		this.dao = dao;
		setTitle("Lekar");
		setSize(800, 300);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		initActions();
		GUI();
	}


	private void GUI() {
		ImageIcon addIcon = new ImageIcon(getClass().getResource("/slike/add.gif"));
		btnAdd.setIcon(addIcon);
		mainToolbar.add(btnAdd);
		ImageIcon editIcon = new ImageIcon(getClass().getResource("/slike/edit.gif"));
		btnEdit.setIcon(editIcon);
		mainToolbar.add(btnEdit);
		ImageIcon deleteIcon = new ImageIcon(getClass().getResource("/slike/remove.gif"));
		btnDelete.setIcon(deleteIcon);
		mainToolbar.add(btnDelete);
		
		ImageIcon pregledIcon = new ImageIcon(getClass().getResource("/slike/medical.png"));
		btnPregled.setIcon(pregledIcon);
		mainToolbar.add(btnPregled);
		add(mainToolbar, BorderLayout.NORTH);
		
		int brojPacijenta = dao.getPacijenti().size();
		String[] zaglavlje = new String[] {"ime","prezime","JMBG","pol","adresa","brojtelefona","korisnicko","lozinka","plata","sluzba","specijalizacija"};
		Object[][] podaci = new Object[brojPacijenta][zaglavlje.length];
		
		for(int i=0; i<dao.getLekari().size(); i++) {
			Lekar lekar = dao.getLekari().get(i);
			podaci[i][0] = lekar.getIme();
			podaci[i][1] = lekar.getPrezime();
			podaci[i][2] = lekar.getJBMG();
			podaci[i][3] = lekar.getPol();
			podaci[i][4] = lekar.getAdresa();
			podaci[i][5] = lekar.getBrojTelefona();			
			podaci[i][6] = lekar.getKorisnicko();
			podaci[i][7] = lekar.getLozinka();
			podaci[i][8] = lekar.getPlata();
			podaci[i][9] = lekar.getSpecijalizacija();	
			podaci[i][10] = lekar.getSluzba();

			
		}
		tableModel = new DefaultTableModel(podaci,zaglavlje);
		lekariTabela = new JTable(tableModel);
		lekariTabela.setRowSelectionAllowed(true);
		lekariTabela.setColumnSelectionAllowed(false);
		lekariTabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lekariTabela.setDefaultEditor(Object.class, null);
		
		JScrollPane scrollPane = new JScrollPane(lekariTabela);
		add(scrollPane, BorderLayout.CENTER);
	}
	
	public void initActions() {
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LekarForma lf = new LekarForma(dao, null);
				lf.setVisible(true);

			}
			
		});
		btnEdit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int red = lekariTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati red");	
				}else {
					String jmbg = lekariTabela.getValueAt(red, 2).toString();
					Lekar lekar = dao.getLekar(jmbg);
					if(lekar!=null) {
						LekarForma lf = new LekarForma(dao, lekar);
						lf.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "greska");
					}
				}
			}
			
		});
		btnDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int red = lekariTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati red");
				}else {
					String jmbg = lekariTabela.getValueAt(red, 2).toString();
					Lekar lekar = dao.getLekar(jmbg);
					if(lekar != null) {
						int izbor = JOptionPane.showConfirmDialog(null, "Da li ste sigurni da zelite da izbrisete lekara", lekar.getJBMG(),JOptionPane.YES_NO_OPTION);
					
						if(izbor == JOptionPane.YES_OPTION) {
							
							DefaultTableModel model = (DefaultTableModel) lekariTabela.getModel();	
							dao.getLekari().remove(lekar);
							model.removeRow(red);
							
						}
					}else {
						JOptionPane.showMessageDialog(null, "greska");
					}
				}
				
			}
			
		});
		btnPregled.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int red = lekariTabela.getSelectedColumn();
				if(red ==-1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati red");
				}else {
					String jmbg = lekariTabela.getValueAt(red, 2).toString();
					Lekar lekar= dao.getLekar(jmbg);
					if(lekar != null) {
						ZakazivanjePregleda pf = new ZakazivanjePregleda(dao, null, lekar);
						pf.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "greska");
					}
					
				}
				
			}
			
		});
	}

}
