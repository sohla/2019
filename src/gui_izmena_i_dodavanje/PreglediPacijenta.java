package gui_izmena_i_dodavanje;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import forme.PacijentPregled;
import modeli.Pregled;
import obrada.DAO;

public class PreglediPacijenta extends JFrame{
	
	private JToolBar mainToolbar = new JToolBar();
	private JButton btnEdit = new JButton();
	private JButton btnPregled = new JButton();
	private JButton btnAdd = new JButton();
	
	private DefaultTableModel tableModel;
	private JTable preglediTabela;
	
	private DAO dao;
	private String jmbg;

	public PreglediPacijenta(DAO dao, String jmbg) {
		this.jmbg = jmbg;
		this.dao = dao;
		setTitle("Pregledi pacijenta");
		setLocationRelativeTo(null);
		setSize(800,300);
		initActions();
		GUI();
	}

	private void GUI() {
		
		ImageIcon editIcon = new ImageIcon(getClass().getResource("/slike/edit.gif"));
		btnEdit.setIcon(editIcon);
		mainToolbar.add(btnEdit);
		
		ImageIcon addIcon = new ImageIcon(getClass().getResource("/slike/add.gif"));
		btnAdd.setIcon(addIcon);
		mainToolbar.add(btnAdd);
		
		add(mainToolbar,BorderLayout.NORTH);
		
		int brojPregleda = dao.getPreglediPacijenta().size();
		String[] zaglavlje = new String[] {"pacijent","lekar","termin","soba","opis","status"};
		Object[][] podaci = new Object[brojPregleda][zaglavlje.length];
		//i<dao.getPreglediPacijenta().size(); 
		//ovako probaj 
		
		for(int i=0; i<dao.getPreglediPacijenta().size(); i++) {
			Pregled pregled = dao.getPreglediPacijenta().get(i);
			podaci[i][0] = pregled.getLekar();
			podaci[i][1] = pregled.getPacijent();
			podaci[i][2] = pregled.getTermin();
			podaci[i][3] = pregled.getSoba();
			podaci[i][4] = pregled.getOpis();
			podaci[i][5] = pregled.getStatus();			
	
		}
		tableModel = new DefaultTableModel(podaci,zaglavlje);
		preglediTabela = new JTable(tableModel);
		preglediTabela.setRowSelectionAllowed(true);
		preglediTabela.setColumnSelectionAllowed(false);
		preglediTabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		preglediTabela.setDefaultEditor(Object.class, null);
		
		JScrollPane scrollPane = new JScrollPane(preglediTabela);
		add(scrollPane, BorderLayout.CENTER);
	}
	
	public void initActions() {
		
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int red = preglediTabela.getSelectedRow();
				if(red==-1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati red");
				}else {
					String jmbg = preglediTabela.getValueAt(red, 0).toString();
					Pregled pregled = dao.getPreglediLekara(jmbg);
					if (pregled != null) {
						PacijentPregled pp = new PacijentPregled(dao, pregled);
						pp.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null,"Greska");
					}
					
				}
				
			}
		});
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				PacijentPregled pp = new PacijentPregled(dao, null);
				pp.setVisible(true);

				
			}
		});
			
		}
		
	}




