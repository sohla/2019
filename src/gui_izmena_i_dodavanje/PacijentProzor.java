package gui_izmena_i_dodavanje;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import forme.MedicinskaSestraForma;
import forme.PacijentForma;
import forme.ZakazivanjePregleda;
import modeli.Pacijent;
import obrada.DAO;

public class PacijentProzor extends JFrame {
	
	private JFrame ZakazivanjePregleda;

	private JToolBar mainToolbar = new JToolBar();
	private JButton btnAdd = new JButton();
	private JButton btnEdit = new JButton();
	private JButton btnDelete = new JButton();
	private JButton btnPregled = new JButton();

	private DefaultTableModel tableModel;
	private JTable pacijentiTabela;
	private DAO dao;

	public PacijentProzor(DAO dao) {
		//this.zk = zk;
		this.dao = dao;
		setTitle("Pacijenti");
		setSize(800, 300);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		initActions();
		GUI();
	}




	private void GUI() {
		ImageIcon addIcon = new ImageIcon(getClass().getResource("/slike/add.gif"));
		btnAdd.setIcon(addIcon);
		mainToolbar.add(btnAdd);
		ImageIcon editIcon = new ImageIcon(getClass().getResource("/slike/edit.gif"));
		btnEdit.setIcon(editIcon);
		mainToolbar.add(btnEdit);
		ImageIcon deleteIcon = new ImageIcon(getClass().getResource("/slike/remove.gif"));
		btnDelete.setIcon(deleteIcon);
		mainToolbar.add(btnDelete);
		ImageIcon pregledIcon = new ImageIcon(getClass().getResource("/slike/medical.png"));
		btnPregled.setIcon(pregledIcon);
		mainToolbar.add(btnPregled);
		add(mainToolbar, BorderLayout.NORTH);
		
		int brojPacijenata = dao.getPacijenti().size();
		String[] zaglavlje = new String[] {"ime","prezime","JMBG","pol","adresa","brojtelefona","korisnicko","lozinka","lekar","broj","datum isteka","kategorija"};
		Object[][] podaci = new Object[brojPacijenata][zaglavlje.length];
		
		for(int i=0; i<dao.getPacijenti().size(); i++) {
			Pacijent pacijent = dao.getPacijenti().get(i);
			podaci[i][0] = pacijent.getIme();
			podaci[i][1] = pacijent.getPrezime();
			podaci[i][2] = pacijent.getJBMG();
			podaci[i][3] = pacijent.getPol();
			podaci[i][4] = pacijent.getAdresa();
			podaci[i][5] = pacijent.getBrojTelefona();			
			podaci[i][6] = pacijent.getKorisnicko();
			podaci[i][7] = pacijent.getLozinka();
			podaci[i][8] = pacijent.getLekar();
			podaci[i][9] = pacijent.getBroj();	
			podaci[i][10] = pacijent.getDatumIsteka();
			podaci[i][11] = pacijent.getKategorija();
			
		}
		tableModel = new DefaultTableModel(podaci,zaglavlje);
		pacijentiTabela = new JTable(tableModel);
		pacijentiTabela.setRowSelectionAllowed(true);
		pacijentiTabela.setColumnSelectionAllowed(false);
		pacijentiTabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		pacijentiTabela.setDefaultEditor(Object.class, null);
		
		JScrollPane scrollPane = new JScrollPane(pacijentiTabela);
		add(scrollPane, BorderLayout.CENTER);
	}
	

	public void initActions() {
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PacijentForma pf = new PacijentForma(dao, null);
				pf.setVisible(true);
			}
			
		});
		btnEdit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int red = pacijentiTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati red");
				}else {
					String jmbg = pacijentiTabela.getValueAt(red, 2).toString();
					Pacijent pacijent = dao.getPacijent(jmbg);
					if(pacijent != null) {
						PacijentForma pf = new PacijentForma(dao, pacijent);
						pf.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "greska");
					}
				}			
			}
			
		});
		btnDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int red = pacijentiTabela.getSelectedRow();
				if(red == -1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati red");
				}else {
					String jmbg = pacijentiTabela.getValueAt(red, 2).toString();
					
					Pacijent pacijent = dao.getPacijent(jmbg);
					if(pacijent != null) {
						int izbor = JOptionPane.showConfirmDialog(null, "Da li ste sigurni da zelite da izbrisete pacijenta", pacijent.getJBMG(),JOptionPane.YES_NO_OPTION);
					
						if(izbor == JOptionPane.YES_OPTION) {
							
							DefaultTableModel model = (DefaultTableModel) pacijentiTabela.getModel();
							dao.getPacijenti().remove(pacijent);
							model.removeRow(red);
							
							
						}
					}else {
						JOptionPane.showMessageDialog(null, "greska");
					}
				}
				
			}
			
		});
		btnPregled.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int red = pacijentiTabela.getSelectedColumn();
				if(red ==-1) {
					JOptionPane.showMessageDialog(null, "Morate izabrati red");
				}else {
					String jmbg = pacijentiTabela.getValueAt(red, 2).toString();
					Pacijent pacijent = dao.getPacijent(jmbg);

					ZakazivanjePregleda pf = new ZakazivanjePregleda(dao, pacijent, null);
					//ZakazivanjePregleda.isShowing();
					//if(!pf.isVisible()) pf.setVisible(true);
					pf.setVisible(true);
				//	print(pf.isShowing());
				}
				
			}
			
		});
	}

}
